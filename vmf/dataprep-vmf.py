# dataprep-vmf.py
# author: Daniel Lima <danielm@usp.br>
import re
import os
import sys
from time import time

import h5py
import numpy as np
import pandas as pd
import scipy.ndimage as ndi

from util import tpr, norm, crop

def vmf(I,X,Y,wt=0.9,ws=0.1):
    ''' returns (index, Localized mri, Mask), center, scale, binary, rbf '''
    # spatial mean and std
    j3 = np.ones((1,3,3,3))/27
    mI = ndi.convolve(I,j3)
    sI = ndi.convolve((I - mI)**2, j3)
    xs = 0.5*mI/mI.max() + 0.5*sI/sI.max()
    # root sum squared temporal derivative
    gI = ndi.gaussian_filter(I,(2,1,2,2))
    dI = ndi.sobel(gI,0)
    EI = (dI ** 2).sum(0)
    z,y,x = EI.shape
    r = np.mgrid[0:(z-1):(z*1j), 0:(y-1):(y*1j), 0:(x-1):(x*1j)]
    # center of mass
    m0 = EI.sum()
    mz = (r[0] * EI).sum()
    my = (r[1] * EI).sum()
    mx = (r[2] * EI).sum()
    c = np.array([mz / m0,
                  my / m0,
                  mx / m0,]).reshape((3,1,1,1))
    # combine and detect scale
    xt = wt*EI/EI.max() + ws*xs/xs.max()
    b = (EI > np.quantile(EI,0.9))
    sb = b.sum() ** 0.333 / np.linalg.norm((z,y,x)) * 3
    #b = ndi.gaussian_filter(b, (1,5,5))
    sr = np.reshape((z,y,x), (3,1,1,1))
    d = np.linalg.norm((r-c)/sr, axis=0)
    t = np.exp(-(d/sb)**2)
    #t[cy-1:cy+1,cx-1:cx+1] = 2*t.max()
    return crop(I,Y,t), c, sb*np.linalg.norm(sr), b, t

def func(d):
    k,I,X,Y = d
    (i,L,M),c,d,b,t = vmf(I/255,X,Y)
    X0, Y0, X1, Y1 = [], [], [], []
    info = []
    for f,xi,yi,li,mi in zip(X, I[X][:,i:], Y[:,i:], L[X], M):
        for z,xj,yj,lj,mj in zip(range(i,i+len(xi)), xi, yi, li, mi):
            if yj.sum() < 25:
                continue
            X0.append(xj)
            Y0.append(yj)
            X1.append(lj)
            Y1.append(mj)
            info.append([k, f, z, tpr(yj,mj)])
    dims = I.shape+tuple(c.ravel())+(d,)
    return k, dims, info, X0, Y0, X1, Y1

df = pd.read_csv('data/stats.csv')
cmrdata = h5py.File('data/cmr-data.h5','r')
vmfdata = h5py.File('data/vmf-data.h5','w')

def data():
    for k,d in cmrdata.items():
        yield k, d['image'][:], d['frames'][:], d['labels'][:]

d1, d2 = [], []
n, t0 = 0, time()
for k,p,q,X0,Y0,X1,Y1 in map(func, data()):
    d1.append((k,) + p)
    d2 += q
    n += 1
    print(k, np.shape(X0), round(n/(time()-t0),2), 'items/s', np.sum(X0), np.sum(Y0), np.sum(X1), np.sum(Y1), end='', flush=1)
    vmfdata.create_dataset(f'img/{k}/x0', data=X0, dtype='u1')
    print('.', end='', flush=1)
    vmfdata.create_dataset(f'img/{k}/x1', data=X1, dtype='u1')
    print('.', end='', flush=1)
    vmfdata.create_dataset(f'img/{k}/y0', data=Y0, dtype='u1')
    print('.', end='', flush=1)
    vmfdata.create_dataset(f'img/{k}/y1', data=Y1, dtype='u1')
    print('.')

d1 = pd.DataFrame(d1, columns='k t z y x cz cy cx r'.split())
d1.to_parquet('data/out-vmf.parquet')

d2 = pd.DataFrame(d2, columns='k f z tpr'.split())
d2.to_parquet('data/out-vmf-tpr.parquet')

# bugs
# DET0005301
# DET0040101

