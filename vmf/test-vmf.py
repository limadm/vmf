# test-vmf.py
# author: Daniel Lima <danielm@usp.br>
import numpy as np
import nibabel as nib
import scipy.ndimage as ndi

I = nib.load('ACDC/patient001/patient001_4d.nii.gz').get_fdata().T
I = I/I.max()
j3 = np.ones((1,3,3,3))
mI = ndi.convolve(I,j3)
sI = ndi.convolve((I - mI)**2, j3)
xs = 0.5*mI/mI.max() + 0.5*sI/sI.max()
# root sum squared temporal derivative
gI = ndi.gaussian_filter(I,(2,1,2,2))
dI = ndi.sobel(gI,0)
EI = (dI ** 2).sum(0)
print(EI.min(), EI.max())
