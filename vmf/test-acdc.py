# test-acdc.py
# author: Daniel Lima <danielm@usp.br>
import re
import sys
import h5py
import numpy as np
import pandas as pd
import scipy.ndimage as ndi
import scipy.ndimage.interpolation as interp
import tensorflow as tf
from tensorflow import keras
from time import time

def norm(x, e=0.001):
    a, b = x.min(), x.max()
    return (x-a+e)/(b-a+e)

def crop(I, Y, t):
    b = t > 0.5
    z = b.max((1,2))
    y = b.max((0,2))
    x = b.max((0,1))
    i,j = [],[]
    for a in [z,y,x]:
        w = np.where(a)[0]
        i.append(w.min())
        j.append(w.max()+1)
    # crop in z
    #L = I[:, i[0]:j[0], i[1]:j[1], i[2]:j[2]]
    #M = Y[:, i[0]:j[0], i[1]:j[1], i[2]:j[2]]
    # no crop in z
    L = I[:,:, i[1]:j[1], i[2]:j[2]]
    M = Y[:,:, i[1]:j[1], i[2]:j[2]]
    return 0, L, M

def vmf(I,X,Y,wt=0.9,ws=0.1):
    ''' returns (index, Localized mri, Mask), center, scale, binary, rbf '''
    # spatial mean and std
    j3 = np.ones((1,3,3,3))
    mI = ndi.convolve(I,j3)
    sI = ndi.convolve((I - mI)**2, j3)
    xs = 0.5*mI/mI.max() + 0.5*sI/sI.max()
    # root sum squared temporal derivative
    gI = ndi.gaussian_filter(I,(2,1,2,2))
    dI = ndi.sobel(gI,0)
    EI = (dI ** 2).sum(0)
    z,y,x = EI.shape
    r = np.mgrid[0:(z-1):(z*1j), 0:(y-1):(y*1j), 0:(x-1):(x*1j)]
    # center of mass
    m0 = EI.sum()
    mz = (r[0] * EI).sum()
    my = (r[1] * EI).sum()
    mx = (r[2] * EI).sum()
    c = np.array([mz / m0,
                  my / m0,
                  mx / m0,]).reshape((3,1,1,1))
    # combine and detect scale
    xt = wt*EI/EI.max() + ws*xs/xs.max()
    b = (EI > np.quantile(EI,0.9))
    sb = b.sum() ** 0.333 / np.linalg.norm((z,y,x)) * 3
    #b = ndi.gaussian_filter(b, (1,5,5))
    sr = np.reshape(EI.shape, (3,1,1,1))
    d = np.linalg.norm((r-c)/sr, axis=0)
    t = np.exp(-(d/sb)**2)
    #t[cy-1:cy+1,cx-1:cx+1] = 2*t.max()
    return crop(I,Y,t), c, sb*np.linalg.norm(sr), b, t

def resize(x):
    h,w = x.shape
    n = max(h,w)
    i,j = (n-h)//2, (n-w)//2
    y = np.empty((n, n, 1))
    y[:,:,:] = x.min()
    y[i:i+h, j:j+w, 0] = x
    y = ndi.zoom(y, (256/n, 256/n, 1))
    if x.dtype == np.uint8:
        return (y > 0.5).astype('u1')
    else:
        a = np.quantile(y.ravel(),   1/255)
        b = np.quantile(y.ravel(), 254/255)
        return norm(np.clip(y, a, b))

def tpr(y,m):
    return round((m.sum()+1) / (y.sum()+1), 4)

def dice(y,p):
    return round(((y*p).sum()*2 + 1) / (y.sum() + p.sum() + 1), 4)

m1 = keras.models.load_model('ACDC_TrainedModel_epi.h5', compile=False)
m2 = keras.models.load_model('ACDC_TrainedModel_endo.h5', compile=False)

def predict(x):
    yp = m1.predict(x) - m2.predict(x)
    return (yp > 0.5).astype('u1')

df = pd.read_csv('stats.csv')
h5 = h5py.File('cmr-data.h5','r')

do = []
n, t0 = 0, time()
for s,f in h5.items():
    for p,d in f.items():
        I,X,Y = d['image'], d['frames'], d['labels']
        (i,L,M),c,d,b,t = vmf(I,X,Y)
        X0, Y0, X1, Y1 = [], [], [], []
        for xi,yi,li,mi in zip(I[X][:,i:], Y[:,i:], L[X], M):
            for xj,yj,lj,mj in zip(xi, yi, li, mi):
                if yj.sum() < 25:
                    continue
                X0.append(resize(xj))
                Y0.append(resize(yj))
                X1.append(resize(lj))
                Y1.append(resize(mj))
        P0 = predict(np.array(X0))
        P1 = predict(np.array(X1))
        n += 1
        for x0,y0,x1,y1,p0,p1 in zip(X0,Y0,X1,Y1,P0,P1):
            print(s, p, tpr(yj,mj), dice(y0,p0), dice(y1,p1))
            do.append([s, p, tpr(yj,mj), dice(y0,p0), dice(y1,p1)])
        print(round(n/(time()-t0),2), 'items/s', file=sys.stderr)

do = pd.DataFrame(do)
do.to_csv('out.csv', columns='s p tpr dice1 dice2'.split(), index=False)

# bugs
# DET0005301
# DET0040101

