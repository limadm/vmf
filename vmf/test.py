# test.py
# author: Daniel Lima <danielm@usp.br>
import sys
import time
import numpy as np
import pandas as pd

print('Usage: python',sys.argv[0],' <dataset> <prep> <testset> <subset>')
print('   dataset:    ACDC LVSC MMs all')
print('   prep:     0 1')
print('   testset:  ACDC LVSC MMs all')
print('   subset:   train valid')
dataset,prep,testset,subset = sys.argv[1:]

from util import load_model, f1s
from data import Data, vmfdata

m = load_model(f'models/{prep}-{dataset}/vgg16.h5')
print('Evaluating',testset,subset,prep)
s = []
for x,y in Data(i=prep, dataset=testset, subset=subset):
    t = m.predict(x).round().astype('u1')
    s += f1s(y,t)
    print('.',end='',flush=1)
print()
print(np.nanmean(s))

