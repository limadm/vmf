# util.py
# author: Daniel Lima <danielm@usp.br>
try:
    import keras
except:
    from tensorflow import keras
import numpy as np
import scipy.ndimage as ndi
from scipy.spatial.distance import dice

def load_model(f):
    return keras.models.load_model(f, compile=False)

def f1(y,t):
    return 1 - dice(y.ravel(), t.ravel())

def tpr(y,m):
    return round((m.sum()+1) / (y.sum()+1), 4)

def norm(x, e=0.001):
    a, b = x.min(), x.max()
    return (x-a+e)/(b-a+e)

def norm255(x):
    return (norm(x)*255).astype('u1')

def crop(I, Y, t):
    b = t > 0.5
    z = b.max((1,2))
    y = b.max((0,2))
    x = b.max((0,1))
    i,j = [],[]
    for a in [z,y,x]:
        w = np.where(a)[0]
        i.append(w.min())
        j.append(w.max()+1)
    # crop in z
    #L = I[:, i[0]:j[0], i[1]:j[1], i[2]:j[2]]
    #M = Y[:, i[0]:j[0], i[1]:j[1], i[2]:j[2]]
    # no crop in z
    L = I[:,:, i[1]:j[1], i[2]:j[2]]
    M = Y[:,:, i[1]:j[1], i[2]:j[2]]
    return 0, L, M

def glue(y):
    t = np.max([yi[0] for yi in y]) + 1
    y = [yi[-1] for yi in sorted(y)]
    return t, np.array(y).reshape((t,-1)+y[0].shape)

def masksplit(i):
    i0 = i.reshape(i.shape[:2])
    i1 = ndi.binary_fill_holes(i0)
    return i1, i1-i0

def f1s(ys,ts):
    s = []
    for y,t in zip(ys,ts):
        y1,y2 = masksplit(y)
        t1,t2 = masksplit(t)
        s += [f1(y1,t1), f1(y2,t2)]
    return s

def l2_kernel(w):
    k = np.mgrid[0:w, 0:w]
    k = np.linalg.norm(k-w//2, axis=0)
    k = k.max() - k
    return (k / k.sum())[None,...]

MYBALL = l2_kernel(7)

def normalized(x):
    x = x / 255
    m = ndi.convolve(x,MYBALL)
    v = (x - m)**2
    s = ndi.convolve(v,MYBALL) **.5 +.001
    z = (x - m +.001) / s
    w = np.clip(z, -2, 2)
    return w*s + m

def categorical(y):
    epi = [2*ndi.binary_fill_holes(yi) - yi for yi in y]
    sep = [keras.utils.to_categorical(yi,3) for yi in epi]
    return np.array(sep)

#def resize(x):
#    h,w = x.shape
#    n = max(h,w)
#    i,j = (n-h)//2, (n-w)//2
#    y = np.empty((n, n, 1))
#    y[:,:,:] = x.min()
#    y[i:i+h, j:j+w, 0] = x
#    y = ndi.zoom(y, (256/n, 256/n, 1))
#    if x.dtype == np.uint8:
#        return (y > 0.5).astype('u1')
#    else:
#        a = np.quantile(y.ravel(),   1/255)
#        b = np.quantile(y.ravel(), 254/255)
#        return norm(np.clip(y, a, b))
