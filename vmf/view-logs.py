# view-logs.py
# author: Daniel Lima <danielm@usp.br>
import sys
from glob import glob
import pandas as pd

print('Usage: python',sys.argv[0],'<prep=0,1> <dataset=ACDC,LVSC,MMs,all>')
i,j = sys.argv[1:]

df = {fn:pd.read_csv(fn) for fn in sorted(glob(f'logs/{i}-{j}/*.csv'))}

ls = sorted([(x, d['f1-score'].max(), d['val_f1-score'].max(), d.time.mean()) for x,d in df.items()], key=lambda x:x[-1], reverse=1)
print('rede,dice,val,time')
for l in ls:
    print(f'{l[0]},{l[1]:.3f},{l[2]:.3f},{l[3]:.1f}')
