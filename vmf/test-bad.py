# test-bad.py
# author: Daniel Lima <danielm@usp.br>
import sys
import time
import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import cv2

print('Plot bad predictions')
print('Usage: python',sys.argv[0],' <dataset> <testset>')
print('   dataset:  ACDC LVSC MMs all')
print('   testset:  ACDC LVSC MMs all')
dataset,testset = sys.argv[1:]

from util import load_model, f1
from data import Data, outdata

m0 = load_model(f'models/0-{dataset}/vgg16.h5')
m1 = load_model(f'models/1-{dataset}/vgg16.h5')
plotpreds = True

kx = np.array([
    [0,1,0],
    [1,1,1],
    [0,1,0]], dtype='u1')

print('Evaluating',testset)
idx, df = 0, []
for (x0,y0),(x1,y1,k) in zip(
        Data(i=0, dataset=testset, subset='valid', bs=8, random_shift=False),
        Data(i=1, dataset=testset, subset='valid', bs=8, random_shift=False, keyed=True)):
    h = outdata[outdata.k == k].iloc[0]
    ox = 2*x0.shape[2]
    sy, sx = x1.shape[1:3]
    dy, dx, err = 0, 0, 1e9
    for i in range(x0.shape[1]-sy):
        for j in range(x0.shape[2]-sx):
            e = np.abs(x0[0, i:i+sy, j:j+sx, 0] - x1[0,...,0]).mean()
            if e < err:
                dy, dx, err = i, j, e
    assert(err == 0.0)
    t0 = m0.predict(x0).round().astype('u1')
    t1 = m1.predict(x1).round().astype('u1')
    t2 = np.zeros_like(t0)
    t2[:,dy:dy+sy,dx:dx+sx,:] = t1
    t1 = t2
    for a, b, x, y in zip(t0, t1, x0, y0):
        df.append([k, f1(y,a), f1(y,b)])
        if f1(y,b) < 0.2:
            x = np.concatenate([x,x,x], axis=1)
            t = np.concatenate([
                y[...,0] - cv2.erode(y, kx),
                a[...,0] - cv2.erode(a, kx),
                b[...,0] - cv2.erode(b, kx),
                ], axis=1)[...,None]
            out = np.concatenate([
                np.where(t > 0, t, x),
                np.where(t > 0, t, x),
                x], axis=-1)
            out = cv2.rectangle(out, (ox+dx,dy), (ox+dx+sx-1,dy+sy-1), (0,1,0))
            plt.imsave(f'outs/out-{idx}-{k}.png', out)
            idx += 1
        print('.', end='', flush=1)

print('\nDone')

df = pd.DataFrame(df, columns=['k','base','vmf'])
