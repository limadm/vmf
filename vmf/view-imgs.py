# view-imgs.py
# author: Daniel Lima <danielm@usp.br>
import re
import numpy as np
import pandas as pd
import nibabel as nib
from pydicom import dcmread
from scipy.io import loadmat
from glob import glob

import matplotlib.pyplot as plt
plt.ion()

X,Y = [],[]

#for p in glob('ACDC/*/*_gt.nii.gz'):
#    y = nib.load(p).get_fdata().T
#    p = p.replace('_gt.','.')
#    x = nib.load(p).get_fdata().T
#    i = len(y)//2
#    X.append(x)
#    Y.append(y)
#    plt.clf()
#    plt.imshow(y[i]/y.max() + x[i]/x.max())
#    plt.pause(0.001)
#
#for p in glob('LVQ/training/*.mat')[:5]:
#    m = loadmat(p)
#    x = m['image'].T
#    y = (m['endo'] + m['epi']).T
#    i = len(y)//2
#    X.append(x)
#    Y.append(y)
#    plt.clf()
#    plt.imshow(y[i]/y.max() + x[i]/x.max())
#    plt.pause(0.001)

for p in glob('LVSC/**/DET*/', recursive=True)[:5]:
    ims = sorted([(int(re.search('_ph([0-9]+)',f)[1])+1,
                   int(re.search('_SA([0-9]+)',f)[1]),
                   dcmread(f),
                   plt.imread(f.replace('.dcm','.png'))
                   )
            for f in glob(p+'/*_SA*.dcm')])
    t,z,x,y = np.transpose(ims)
    x = x.reshape(t[-1],z[-1],-1)
    y = y.reshape(t[-1],z[-1],-1)
    for xi,yi in zip(x,y):
        plt.clf()
        plt.imshow(xi[len(xi)//2,0].pixel_array/xi[0,0].pixel_array.max() + yi[len(yi)//2,0][...,0])
        plt.pause(0.001)
