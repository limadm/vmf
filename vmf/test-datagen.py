# test-datagen.py
# author: Daniel Lima <danielm@usp.br>
from data import Data

for dataset in ['ACDC','MMs','LVSC','all']:
    for subset in ['train','valid']:
        for i in [0,1]:
            n = sum((1 for x,y in Data(i,dataset,subset)))
            print(dataset, subset, i, n)
