# train.py
# author: Daniel Lima <danielm@usp.br>
import sys
import time
import pandas as pd
try:
    import keras
    from keras.utils import generic_utils
except:
    from tensorflow import keras
import segmentation_models as sm
import matplotlib.pyplot as plt
plt.ion()
from data import Data, sizes


#d1 = pd.read_parquet('out-vmf.parquet')
#d2 = pd.read_parquet('out-vmf-tpr.parquet')
#df = []
#for k in d:
#    i = d[k]
#    df.append(i['y1'].shape)
#df = pd.DataFrame(df, columns=['i','x','y'])

def Model(base):
    m = sm.Unet(base, input_shape=(None,None,1), encoder_weights=None, classes=3, activation='softmax')
    m.summary()
    return m

    x = keras.layers.Conv2D(3, 1, kernel_initializer='ones', bias_initializer='zeros')(i)#, trainable=False)(i)
    o = sm.Unet(base, input_shape=(None,None,3), encoder_weights='imagenet')(x)#, encoder_freeze=True)(x)
    m = keras.models.Model(i, o)
    m.summary()
    return m

class Timer(keras.callbacks.Callback):
    def __init__(self, logs={}):
        self.logs=[]
    def on_epoch_begin(self, epoch, logs={}):
        self.t0 = time.time()
    def on_epoch_end(self, epoch, logs={}):
        self.logs.append(time.time() - self.t0)

m = Model('vgg16')

d = Data(i=0, dataset='ACDC', subset='test', epochs=1)

for x,y in d:
    for xi, yi in zip(x,y):
        plt.clf()
        plt.imshow(xi, cmap='gray')
        plt.imshow(yi, alpha=0.4)
        plt.pause(0.001)
        1/0


1/0

epochs = 30

print('Usage: python',sys.argv[0],'<dataset...>')
print('  dataset: ACDC LVSC MMs all')
if len(sys.argv) > 1:
    args = sys.argv[1:]
else:
    args = sizes.keys()

for dataset in args:
    Nt, Nv = sizes[dataset]
    #for i in [0,1]:
    for i in [0]:
        base = 'vgg16'
        print('Training',base,'on',i,dataset)
        m = Model(base)
        m.compile('nadam', #keras.optimizers.SGD(lr=0.01, nesterov=True, momentum=0.9, decay=1e-6),
                sm.losses.bce_dice_loss,
                metrics=[sm.metrics.FScore(), sm.metrics.IoU()])
        timer = Timer()
        saver = keras.callbacks.ModelCheckpoint(f'models/{i}-{dataset}/{base}.h5',
                        monitor='val_f1-score', mode='max', save_best_only=True)
        #h = m.fit(Data(i=i, dataset=dataset), epochs=epochs)
        train = Data(i=i, dataset=dataset, subset='train', epochs=epochs+1)
        valid = Data(i=i, dataset=dataset, subset='valid', epochs=epochs+1)
        h = m.fit(train, epochs=epochs, steps_per_epoch=Nt,
                validation_data=valid, validation_steps=Nv,
                callbacks=[timer,saver],)
        l = h.history
        l['time'] = timer.logs
        pd.DataFrame(l).to_csv(f'logs/{i}-{dataset}/{base}.csv')

