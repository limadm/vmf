# test-chisq.py
# author: Daniel Lima <danielm@usp.br>
import sys
import time
import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

print('Chi-squared comparison')
print('Usage: python',sys.argv[0],' <dataset> <testset>')
print('   dataset:  ACDC LVSC MMs all')
print('   testset:  ACDC LVSC MMs all')
dataset,testset = sys.argv[1:]

from util import load_model, f1
from data import Data, outdata

m0 = load_model(f'models/0-{dataset}/vgg16.h5')
m1 = load_model(f'models/1-{dataset}/vgg16.h5')
plotpreds = True

print('Evaluating',testset)
cm = np.zeros((2,2), dtype=int)
for (x0,y0),(x1,y1,k) in zip(
        Data(i=0, dataset=testset, subset='valid', bs=8, random_shift=False),
        Data(i=1, dataset=testset, subset='valid', bs=8, random_shift=False, keyed=True)):
    h = outdata[outdata.k == k].iloc[0]
    sy, sx = x1.shape[1:3]
    dy, dx, err = 0, 0, 1e9
    for i in range(x0.shape[1]-sy):
        for j in range(x0.shape[2]-sx):
            e = np.abs(x0[0, i:i+sy, j:j+sx, 0] - x1[0,...,0]).mean()
            if e < err:
                dy, dx, err = i, j, e
    assert(err == 0.0)
    t0 = m0.predict(x0).round().astype('u1')
    t1 = m1.predict(x1).round().astype('u1')
    t2 = np.zeros_like(t0)
    t2[:,dy:dy+sy,dx:dx+sx,:] = t1
    t1 = t2
    cm += confusion_matrix(t0.ravel(), t1.ravel())
    print('.', end='', flush=1)

print('\nDone')
print(cm)
r = sm.stats.mcnemar(cm, exact=False, correction=True)
print(r.statistic, r.pvalue)

