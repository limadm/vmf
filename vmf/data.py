# data.py
# author: Daniel Lima <danielm@usp.br>
import time
import h5py
import numpy as np
import pandas as pd
import scipy.ndimage as ndi
from util import normalized, categorical

outdata = pd.read_parquet('data/out-vmf.parquet')
tprdata = pd.read_parquet('data/out-vmf-tpr.parquet')
vmfdata = h5py.File('data/vmf-data.h5','r')
imgdata = vmfdata['img']

def identity(*args):
    return args

def augment(x, y):
    if np.random.random() < .5:
        return ndi.gaussian_filter(x, (0,1,1)),\
               ndi.gaussian_filter(y, (0,1,1))
    return x, y

sizes = {'ACDC':[ 122,  39],
         'MMs' :[ 360, 121],
         'LVSC':[1162, 389],
         'all' :[1604, 589]}

def Data(i=1, dataset='all', subset='train', split=0.25, bs=16, epochs=1, random_shift=True, keyed=None):
    xi = f'x{i}'
    yi = f'y{i}'
    ks = list(imgdata.keys())
    if dataset != 'all':
        ks = list(filter(lambda x: dataset in x, ks))
    print(f'Found {len(ks)}, ', end='')
    N = int(len(ks) * (1-split))
    if subset.startswith('train'):
        ks = ks[:N]
        T = augment
    else:
        ks = ks[N:]
        T = identity
    print(f'loading {len(ks)} exams...')
    for epoch in range(epochs):
        for k in ks:
            f = imgdata[k]
            if len(f[yi]) == 0:
                continue
            X,Y = f[xi], f[yi]
            s = X.shape
            for m in range(0,s[0],bs):
                if s[1]%32 != 0 or s[2]%32 != 0:
                    i1, i2 = s[1]%32//2, s[2]%32//2
                    if random_shift:
                        i1, i2 = np.random.randint(0, 2*i1+1), np.random.randint(0, 2*i2+1)
                    j1, j2 = i1+(s[1] & 0xFFE0), i2+(s[2] & 0xFFE0)
                    x,y = X[m:m+bs,i1:j1,i2:j2], Y[m:m+bs,i1:j1,i2:j2].astype('f4')
                else:
                    x,y = X[m:m+bs], Y[m:m+bs].astype('f4')
                x,y = T(x,y)
                x = normalized(x)
                y = categorical(y)
                if keyed:
                    yield x, y, k
                else:
                    yield x, y

