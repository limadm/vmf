# dataprep-cmr.py
# author: Daniel Lima <danielm@usp.br>
import os
import re
import sys
import h5py
import numpy as np
import pandas as pd
import nibabel as nib
import matplotlib.pyplot as plt
from pydicom import dcmread
from scipy.io import loadmat
from glob import glob
from util import norm255

paths = {}

for f in sorted(glob('data/ACDC/pat*')):
    paths[('ACDC',f[-10:])] = f
for f in sorted(glob('data/LVSC/*training*/DET*')):
    paths[('LVSC',f[-10:])] = f
for f in sorted(glob('data/MMs/**/*_gt.nii.gz', recursive=True)):
    paths[('MMs',f[-19:-13])] = f
#for f in sorted(glob('data/MMs2/**/*_gt.nii.gz', recursive=True)):
#    paths[('MMs',f[-19:-13])] = f

def imgload(row):
    f = paths.get((row.s, row.p))
    print(f'loading {row.s} {row.p}', file=sys.stderr)
    err = None
    try:
        if row.s == 'ACDC':
            p = glob(f'{f}/*_4d.nii.gz')
            assert(len(p) == 1)
            hdr = nib.load(p[0])
            img = hdr.get_fdata().T
            x, y = [], []
            for p in sorted(glob(f'{f}/*_gt.nii.gz')):
                g = re.search('frame([0-9]+)', p)[1]
                x.append(int(g)-1)
                y.append((nib.load(p).get_fdata() == 2).astype('u1').T)
            return norm255(img), x, np.array(y)
        elif row.s == 'MMs':
            hdr = nib.load(f)
            y = (nib.load(f).get_fdata() == 2).astype('u1').T
            img = nib.load(f.replace('_gt','')).get_fdata().T
            x = list(np.where(y.sum((1,2,3)) > 0)[0])
            y = y[x]
            return norm255(img), x, y
        elif row.s == 'LVSC':
            files = {}
            i, y = [], []
            for g in glob(f+'/*_SA*.png'):
                m = re.search('DET[0-9]+_SA([0-9]+)_ph([0-9]+).png', g)
                xi = dcmread(g.replace('.png','.dcm')).pixel_array
                yi = plt.imread(g)[...,0].astype('u1')
                i.append((int(m[2]), int(m[1])-1, xi))
                y.append((int(m[2]), int(m[1])-1, yi))
            t, i = glue(i)
            return norm255(i), list(range(t)), glue(y)[1]
    except Exception as e:
        err = e
    raise Exception(f'cannot load {row.s} {row.p}: {err}')

df = pd.read_csv('data/stats.csv')
if os.path.exists('data/cmr-data.h5'):
    print('data/cmr-data.h5 exists, aborting')
else:
    with h5py.File('data/cmr-data.h5','w') as h5:
        i = 0
        for _,r in df.sample(len(df), random_state=1238172).iterrows():
            try:
                I,X,Y = imgload(r)
                h5.create_dataset(f'/{i:03d}_{r.s}_{r.p}/image',  data=I, dtype='u1')
                h5.create_dataset(f'/{i:03d}_{r.s}_{r.p}/frames', data=X, dtype='u1')
                h5.create_dataset(f'/{i:03d}_{r.s}_{r.p}/labels', data=Y, dtype='u1')
                i += 1
            except Exception as e:
                print('skipping patient:', str(e), file=sys.stderr)

