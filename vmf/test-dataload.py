# test-dataload.py
# author: Daniel Lima <danielm@usp.br>
import h5py
from time import time

h5 = h5py.File('cmr-data.h5','r',track_order=True)

n, t0 = 0, time()
for k,d in h5.items():
    n += 1
    print(k, d['image'][:].sum(), d['frames'][:].sum(), d['labels'][:].sum(), n/(time()-t0))

