# stats.py
# author: Daniel Lima <danielm@usp.br>
import re
import pandas as pd

try:
    df = pd.read_csv('stats.csv')
except:
    import nibabel as nib
    from pydicom import dcmread
    from scipy.io import loadmat
    from glob import glob

    df = []

# # LVQ
# for g in sorted(glob('LVQ/training/*.mat', recursive=True)):
#     i = loadmat(g)
#     s = list(i['image'].shape)
#     p = float(i['pix_spacing'])
#     df.append(['LVQ', g.replace('\\','/').split('/')[-1][:-4]] + s[:2] + [1] + s[-1:] + [p]*2 + [0, None])
#
# # SCD
# for g in sorted(glob('SCD/Sun*/**/*0001.dcm', recursive=True)):
#     g = g.replace('\\','/')
#     f = g.split('/')
#     p = f[-3]
#     i = dcmread(g)
#     df.append([
#         'SCD', p,
#         i.Columns,
#         i.Rows,
#         i.ImagesInAcquisition//i.CardiacNumberOfImages,
#         i.CardiacNumberOfImages,
#         i.PixelSpacing[1],
#         i.PixelSpacing[0],
#         i.SliceThickness,
#         i.SpacingBetweenSlices,])

# ACDC
    for g in sorted(glob('ACDC/**/*4d.nii.gz', recursive=True)):
        i = nib.load(g)
        h = i.header
        df.append(['ACDC', g.replace('\\','/').split('/')[-2]] + list(i.shape) + list(h['pixdim'][1:4]) + [None])
# LVSC
    d1 = []
    for g in sorted(glob('LVSC/**/*.png', recursive=True))[20:]:
        g = g.replace('\\','/')
        path = g.split('/')
        p, f = path[-2:]
        t = int(re.search('_ph([0-9]+)',f)[1])+1
        m = re.search('_([LS]A)([0-9]+)_',f)
        d1.append([g, path[-2], m[1], int(m[2]), t])

    d1 = pd.DataFrame(d1, columns='file pat ax z t'.split())
    for _,g in d1[d1.ax=='SA'].groupby('pat'):
        f,p,a,z,t = g.max()
        i = dcmread(f.replace('.png','.dcm'))
        xy = [i.Columns, i.Rows]
        dxy = [i.PixelSpacing[1], i.PixelSpacing[0]]
        dz = [float(i.SliceThickness)]
        df.append(['LVSC',p]+xy+[z,t]+dxy+dz+[None])

# MMs
    for g in sorted(glob('MMs/**/*gt.nii.gz', recursive=True)):
        i = nib.load(g)
        h = i.header
        df.append(['MMs', g.replace('\\','/').split('/')[-2]] + list(i.shape) + list(h['pixdim'][1:4]) + [None])

    df = pd.DataFrame(df, columns='s p x y z t dx dy dz sz'.split())
    df.to_csv('stats.csv', index=False)

df = pd.read_csv('stats.csv')
print(df.groupby('s').min())
print(df.groupby('s').max())
print(df.s.value_counts())
